import java.io.FileNotFoundException;

public class Solution {
	
	private DictionaryReader reader;
	private WordsFamily family;
	
	public Solution(String path) throws FileNotFoundException {
		reader = new DictionaryReader(path);
		family = new WordsFamily();
	}
	
	public String solvePuzzle(String sourceWord, String destWord){
		return family.getFamilyTree(reader.getDictionary().get(sourceWord.length()), sourceWord, destWord);
	}
}
