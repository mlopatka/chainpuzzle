import java.io.FileNotFoundException;

public class Main {
	
	public static void main(String[] args) throws FileNotFoundException{
		Solution puzzleSoultion = new Solution("resources/wordlist.txt");
		System.out.println(puzzleSoultion.solvePuzzle("ruby", "code"));
	}

}
