import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class DictionaryReader {
	
	private Scanner scanner;
	
	public DictionaryReader(String path) throws FileNotFoundException {
		scanner = new Scanner(new File(path));
	}
	
	public Map<Integer, Set<String>> getDictionary(){
		Map<Integer, Set<String>> dictionary = new HashMap(); 
		while(scanner.hasNextLine()){
			String tmp = scanner.nextLine().toLowerCase();
			if(!dictionary.containsKey(tmp.length())) dictionary.put(tmp.length(), new HashSet<String>());
			dictionary.get(tmp.length()).add(tmp);
		}
		
		return dictionary;
	}
}
