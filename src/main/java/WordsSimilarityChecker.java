
public class WordsSimilarityChecker {

	public int getDifferentCharsCount(String sourceWord, String destWord){
		if(sourceWord.length() != destWord.length()) return -1;
			
		int score = 0;

		for (int i = 0; i < sourceWord.length(); i++) {
			if (sourceWord.charAt(i) == destWord.charAt(i))
				++score;
		}

		return sourceWord.length() - score;
	}
}
