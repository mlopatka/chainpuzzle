
public class DictionaryWord {
	
	private DictionaryWord parent;
	private String word;
	private int remainingChaneges;
	private int step;
	private boolean childernSearched;
	
	public DictionaryWord(String word){
		this.word = word;
		step = 0;
		childernSearched = false;
	}
	
	public DictionaryWord(String word, int remainingChaneges){
		this.word = word;
		this.remainingChaneges = remainingChaneges;
		step = 0;
		childernSearched = false;
	}
	
	public DictionaryWord(DictionaryWord parent, String word, int remainingChaneges){
		this.parent = parent;
		this.word = word;
		this.remainingChaneges = remainingChaneges;
		step = parent.getStep() + 1;
		childernSearched = false;
	}

	public String getWord() {
		return word;
	}

	public int getRemainingChanges() {
		return remainingChaneges;
	}
	
	public DictionaryWord getParent(){
		return parent;
	}
	
	public int getStep(){
		return step;
	}
	
	public int getSumOfStepsAndRemaingChanges(){
		return step + remainingChaneges;
	}
	
	public boolean isChildernSearched() {
		return childernSearched;
	}

	public void setChildernSearched(boolean childernSearched) {
		this.childernSearched = childernSearched;
	}

	@Override
	public String toString() {
		return "Word " + word + " remaining " + remainingChaneges + " changes after " + step + " steps" ;
	}
}
