import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

public class FamiliarWordsFinder {
	
	private WordsSimilarityChecker checker;
	
	public FamiliarWordsFinder(){
		checker = new WordsSimilarityChecker();
	}
	
	public SortedSet<DictionaryWord> findFamiliarWords(DictionaryWord sourceWord, String destWord, Set<String> wordsList,
			List<String> forbiddenWords){
		sourceWord.setChildernSearched(true);
		SortedSet<DictionaryWord> matchingSet = new TreeSet<>(
				Comparator.comparing(DictionaryWord::getSumOfStepsAndRemaingChanges).thenComparing(DictionaryWord::getWord));
		
		for (String wordCandidate : wordsList) {
			if (checker.getDifferentCharsCount(sourceWord.getWord(), wordCandidate) == 1 && !forbiddenWords.contains(wordCandidate)) {
				matchingSet.add(new DictionaryWord(sourceWord, wordCandidate, checker.getDifferentCharsCount(destWord, wordCandidate)));
			}
		}

		matchingSet.stream().forEach(n -> forbiddenWords.add(n.getWord()));
		return matchingSet;
	}
	
}
