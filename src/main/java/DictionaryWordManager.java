
public class DictionaryWordManager {

	public String getFamily(DictionaryWord sourceWord){
		StringBuilder wordFamily = new StringBuilder("");
		while(sourceWord!=null){
			wordFamily.insert(0, sourceWord.getWord() + "\n");
			sourceWord = sourceWord.getParent();
		}
		
		return wordFamily.toString();
	}
	
}
