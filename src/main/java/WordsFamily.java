import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

public class WordsFamily {
	
	private FamiliarWordsFinder finder;
	private DictionaryWordManager manager;
	private SortedSet<DictionaryWord> matchingWords;
	private SortedSet<DictionaryWord> candidates;
	private List<String> forbiddenWords;
	
	private final String chainNotPossible = "No chain found";
	private final String wordsDifferentLength = "%s is other size than %s";
	
	public WordsFamily(){
		matchingWords = new TreeSet<>(Comparator.comparing(DictionaryWord::getSumOfStepsAndRemaingChanges).thenComparing(DictionaryWord::getWord));
		candidates = new TreeSet<>(Comparator.comparing(DictionaryWord::getSumOfStepsAndRemaingChanges).thenComparing(DictionaryWord::getWord));
		forbiddenWords = new ArrayList<>();
		finder = new FamiliarWordsFinder();
		manager = new DictionaryWordManager();
		
	}
	
	public String getFamilyTree(Set<String> wordsList, String sourceWord, String destWord){
		int initWordsDifference = new WordsSimilarityChecker().getDifferentCharsCount(sourceWord, destWord);
		if(initWordsDifference == 0) return sourceWord;
		if(initWordsDifference < 0) return String.format(wordsDifferentLength, sourceWord, destWord);
		
		matchingWords.add(new DictionaryWord(sourceWord, initWordsDifference));
		forbiddenWords.add(sourceWord);
		
		while(matchingWords.first().getRemainingChanges() != 1){
			int best = matchingWords.first().getSumOfStepsAndRemaingChanges();
			matchingWords.stream().filter(n -> n.getSumOfStepsAndRemaingChanges() <= best)
				.forEach(n -> candidates.addAll(finder.findFamiliarWords(n, destWord, wordsList, forbiddenWords)));
			matchingWords.removeIf(n->n.isChildernSearched());
			matchingWords.addAll(candidates);
			candidates.clear();
			if(matchingWords.isEmpty()) return chainNotPossible;
		}
		
		return manager.getFamily(matchingWords.first()) + destWord;
	}

}
