import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class WordsSimilarityCheckerTest {

	@Test
	public void getDifferentCharsCount_sameWords_returnZero(){
		WordsSimilarityChecker checker = new WordsSimilarityChecker();
		int count = checker.getDifferentCharsCount("Word", "Word");
		assertEquals(0, count);
	}
	
	@Test
	public void getDifferentCharsCount_differentWords_returnNumber(){
		WordsSimilarityChecker checker = new WordsSimilarityChecker();
		int count = checker.getDifferentCharsCount("Door", "Word");
		assertEquals(3, count);
	}
	
	@Test
	public void getDifferentCharsCount_differentLengthWordsBiggerFirst_returnNumber(){
		WordsSimilarityChecker checker = new WordsSimilarityChecker();
		int count = checker.getDifferentCharsCount("World", "Word");
		assertEquals(-1, count);
	}
	
	@Test
	public void getDifferentCharsCount_differentLengthWordsBiggerLast_returnNumber(){
		WordsSimilarityChecker checker = new WordsSimilarityChecker();
		int count = checker.getDifferentCharsCount("Word", "World");
		assertEquals(-1, count);
	}
}
