import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class DictionaryWordManagerTest {

	
	@Test
	public void getFamily_noneAncestors_returnSourceWord(){
		DictionaryWordManager manager = new DictionaryWordManager();
		DictionaryWord sourceWord = new DictionaryWord("word");
		
		String family = manager.getFamily(sourceWord);
		assertEquals("word\n", family);
	}
	
	@Test
	public void getFamily_fewAncestors_returnFamily(){
		DictionaryWordManager manager = new DictionaryWordManager();
		DictionaryWord sourceWord = new DictionaryWord("word");
		DictionaryWord ancestorWord = new DictionaryWord(sourceWord, "work", 1);
		DictionaryWord destWord = new DictionaryWord(ancestorWord, "fork", 0);
		
		String family = manager.getFamily(destWord);
		assertEquals("word\nwork\nfork\n", family);
	}
	
	@Test
	public void getFamily_emptyWord_returnNewLine(){
		DictionaryWordManager manager = new DictionaryWordManager();
		DictionaryWord sourceWord = new DictionaryWord("");
		
		String family = manager.getFamily(sourceWord);
		assertEquals("\n", family);
	}
}
