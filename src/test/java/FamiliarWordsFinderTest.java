import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;

import org.junit.Before;
import org.junit.Test;

public class FamiliarWordsFinderTest {

	Set<String> wordsList;
	List<String> forbiddenWordsList;
	
	@Before
	public void initialize(){
		wordsList = new HashSet<>();
		forbiddenWordsList = new ArrayList<>();
	}
	
	@Test
	public void findFamiliarWords_emptyWord_listIsEmpty(){
		FamiliarWordsFinder finder = new FamiliarWordsFinder();
		DictionaryWord sourceWord = new DictionaryWord("");
		wordsList.add("");
		forbiddenWordsList.add("");
		
		SortedSet<DictionaryWord> matchingWords = finder.findFamiliarWords(sourceWord, "", wordsList, forbiddenWordsList);
		assertTrue(matchingWords.isEmpty());
	}
	
	@Test
	public void findFamiliarWords_ignoreForbiddenWords_listIgnoreForbiddenWord(){
		FamiliarWordsFinder finder = new FamiliarWordsFinder();
		DictionaryWord sourceWord = new DictionaryWord("");
		wordsList.add("lord");
		wordsList.add("work");
		wordsList.add("loop");
		wordsList.add("word");
		wordsList.add("worm");
		forbiddenWordsList.add("word");
		forbiddenWordsList.add("worm");
		
		SortedSet<DictionaryWord> matchingWords = finder.findFamiliarWords(sourceWord, "york", wordsList, forbiddenWordsList);
		assertFalse(matchingWords.contains("worm"));
	}
	
	@Test
	public void findFamiliarWords_regularWord_listContainAllMatchingElements(){
		FamiliarWordsFinder finder = new FamiliarWordsFinder();
		DictionaryWord sourceWord = new DictionaryWord("word");
		wordsList.add("lord");
		wordsList.add("work");
		wordsList.add("loop");
		wordsList.add("word");
		wordsList.add("worm");
		forbiddenWordsList.add("word");
		forbiddenWordsList.add("worm");
		
		SortedSet<DictionaryWord> matchingWords = finder.findFamiliarWords(sourceWord, "york", wordsList, forbiddenWordsList);
		assertEquals(2, matchingWords.size());
	}
	
	@Test
	public void findFamiliarWords_returnCollectionFirstElementMostMatchingDestWord_validFirstElement(){
		FamiliarWordsFinder finder = new FamiliarWordsFinder();
		DictionaryWord sourceWord = new DictionaryWord("word");
		wordsList.add("lord");
		wordsList.add("work");
		wordsList.add("loop");
		wordsList.add("word");
		wordsList.add("worm");
		forbiddenWordsList.add("word");
		forbiddenWordsList.add("worm");
		
		SortedSet<DictionaryWord> matchingWords = finder.findFamiliarWords(sourceWord, "york", wordsList, forbiddenWordsList);
		assertEquals("work",matchingWords.first().getWord());
	}
	
	@Test
	public void findFamiliarWords_notFamiliarWordNotContain_validReturnListElements(){
		FamiliarWordsFinder finder = new FamiliarWordsFinder();
		DictionaryWord sourceWord = new DictionaryWord("");
		wordsList.add("lord");
		wordsList.add("work");
		wordsList.add("loop");
		wordsList.add("word");
		wordsList.add("worm");
		forbiddenWordsList.add("word");
		forbiddenWordsList.add("worm");
		
		SortedSet<DictionaryWord> matchingWords = finder.findFamiliarWords(sourceWord, "york", wordsList, forbiddenWordsList);
		assertFalse(matchingWords.contains("loop"));
	}
}
