import java.io.FileNotFoundException;
import java.util.Map;
import java.util.Set;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertNotNull;

public class DictionaryReaderTest {
	
	@Rule
	public ExpectedException expectedException = ExpectedException.none();
	
	@Test
	public void getDictionary_notEmptyTextFile_returnNotEmptyCollection() throws FileNotFoundException{
		DictionaryReader reader = new DictionaryReader("src/test/resources/NotEmptyDictionary");
		Map<Integer, Set<String>> dictionary = reader.getDictionary();
		assertFalse(dictionary.isEmpty());
	}
	
	@Test
	public void getDictionary_emptyTextFile_returnNotNullCollection() throws FileNotFoundException{
		DictionaryReader reader = new DictionaryReader("src/test/resources/EmptyDictionary");
		Map<Integer, Set<String>> dictionary = reader.getDictionary();
		assertNotNull(dictionary);
	}
	
	@Test
	public void getDictionary_nonExistingFile_receiveException() throws FileNotFoundException{
		expectedException.expect(FileNotFoundException.class);
		new DictionaryReader("src/test/resources/MissingDictionary");
	}
	
	@Test
	public void getDictionary_image_returnEmptyCollection() throws FileNotFoundException{
		DictionaryReader reader = new DictionaryReader("src/test/resources/Image.png");
		Map<Integer, Set<String>> dictionary = reader.getDictionary();
		assertTrue(dictionary.isEmpty());
	}
}
