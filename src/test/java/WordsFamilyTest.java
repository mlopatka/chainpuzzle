import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class WordsFamilyTest {
	
	Set<String> wordsList;
	
	@Before
	public void initialize(){
		wordsList = new HashSet<>();
	}
	
	@Test
	public void getFamilyTree_emptyWords_returnEmptyWord(){
		WordsFamily family = new WordsFamily();
		String chain = family.getFamilyTree(wordsList, "", "");
		assertEquals("", chain);
	}
	
	@Test
	public void getFamilyTree_sameWords_returnWord(){
		WordsFamily family = new WordsFamily();
		wordsList.add("word");
		wordsList.add("lord");
		String chain = family.getFamilyTree(wordsList, "word", "word");
		assertEquals("word", chain);
	}
	
	@Test
	public void getFamilyTree_chainNotPossible_returnConstant(){
		WordsFamily family = new WordsFamily();
		wordsList.add("word");
		wordsList.add("lord");
		wordsList.add("loop");
		String chain = family.getFamilyTree(wordsList, "word", "fork");
		assertEquals("No chain found", chain);
	}
	
	@Test
	public void getFamilyTree_emptyDictionary_returnConstant(){
		WordsFamily family = new WordsFamily();
		String chain = family.getFamilyTree(wordsList, "word", "fork");
		assertEquals("No chain found", chain);
	}
	
	@Test
	public void getFamilyTree_wordsDifferentLength_returnConstant(){
		WordsFamily family = new WordsFamily();
		String sourceWord = "word", destWord = "world";
		String chain = family.getFamilyTree(wordsList, sourceWord, destWord);
		assertEquals(sourceWord + " is other size than " + destWord, chain);
	}
	
	@Test
	public void getFamilyTree_validWords_returnValidChain(){
		WordsFamily family = new WordsFamily();
		wordsList.add("word");
		wordsList.add("work");
		String sourceWord = "word", destWord = "work";
		String chain = family.getFamilyTree(wordsList, sourceWord, destWord);
		assertEquals(sourceWord + "\n" + destWord, chain);
	}
	
	@Test
	public void getFamilyTree_validWords_returnExpandedValidChain(){
		WordsFamily family = new WordsFamily();
		wordsList.add("word");
		wordsList.add("work");
		wordsList.add("fork");
		wordsList.add("form");
		wordsList.add("loop");
		wordsList.add("lord");
		String sourceWord = "word", destWord = "form";
		String chain = family.getFamilyTree(wordsList, sourceWord, destWord);
		assertEquals(sourceWord + "\n" + "work" + "\n" + "fork" + "\n" + destWord, chain);
	}
	
}
